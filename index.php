<?php
declare(strict_types=1);
//Data Types & Type casting
$completed=true;
// bool -> True or False
$score=75;
// int -> inteiros
$price=0.99;
// float -> decimais
 $greeting='Olá Mauro';
//string-> letras and gang

echo $completed . '<br />' . $score . '<br />' . $price . '<br />' . $greeting , '<BR />';

var_dump($completed); //saber o tipo do valor e o valor da variavel
echo '<br />' . gettype($price) . '<br />'; //Saber o tipo da variavel

#Arrays
$empresa=[]; //Array vazio
$empresas=[1,2,3,0.5,'A','b',true];

print_r($empresas);
echo '<br />' ;

//TypeHinting -> Php tries to convert the variables and assumir outra cenas
function sum(int $x,$y){//typeversion typehinting
    //I can overwrite the parametros aqui
    var_dump($x,$y);//imprime o tipo de dado e o dado que tá lá
    return $x+$y;
}
echo sum(5,2);

//strict type and type hinting

echo '<br />';


function somar(float $x, float $y){
    return $x + $y;
//porcausa do declaration type ele só  irá funcionar os floats Dio recommends doing it becaus
//qquualidade de code, quais tipos aceitam e evitar bugs
}
$sum =sum(3,2);
echo $sum . '<br />';
var_dump($sum);
/*converter
$c=(int)'5'; */
?>